# frozen_string_literal: true

if defined?(Devise::Mailer)
  class Hancock::DeviseMailer < Devise::Mailer
    
    include Hancock::Decorators::DeviseMailer

  end
end